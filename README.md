# catchment-generator
Project for automatically generate catchment based on river,channel and DEM data

## Supported Environment
- Python 2.7
- GRASS GIS 7.6
- MAC OS, Linux or Docker


## How to Run?
First of all, please create a folder to put input data(DEM, SHP, etc.)
This folder is your data storage. You should put DEM, shp, etc. there..
```
cd 
mkdir storage  
cd ../
```

Then, clone the source code:
```
git clone https://gitlab.com/h2beaver/catchment-generator.git
cd catchment-generator
```

For the next step, there are some options for the deployment environment. We recommend to deploy catchment-generator via Docker but it's depened on your scenario.

### Option 1: Deploy via Docker
Build a image based on Dockerfile.
````
docker build -t catchment-generator .
````

Run the container with several arguments

```
docker run  -v ../storage:storage catchment-generator App.py -d "97LanYang(5mDEM)(ArcGis)_(dig).asc" -c channel_lanyang.shp -p h2 -h 30 -w 200 -b "2711707;2704385;295588;285588"
```
or 

```
docker run  -v ../storage:storage catchment-generator App.py -d "97LanYang(5mDEM)(ArcGis)_(dig).asc" -c channel_lanyang.shp -p h2 -h 30 -w 200 -s c2560_tw97.shp
```

In which, to work with data on the host system within the docker container, using ```-v``` argument to mount the directory. 

For more details, you can run help argument with App.py.
```
docker run  -v ~/h2/storage:/storage catchment-generator App.py --help
```

````
Usage:
App.py
 -d <dem_file_name>
 -c <channel_shapefile_name>

Optional:
 -p <prefix name of ouput file, defaut is h2_>
 -t <threshold for catchment (default is 30000)>
 -m <max slope length of catchment (default is 1000)>
 -h <depth for channel (default is 20)>
 -w <width for channel in meter (default is 300)>
 -b <boundary coordinates for computation. Foramt should be "N;S;E;W">
 -s <boundary shp for computation>
````

### Option 2: Deploy via physical machine
Currently, catchment-generator supports to be deployed in MAC OS and Linux environment.
#### 1. Install GRASS7
GRASS GIS software is necessary for catchment-generator.  The installation steps are variance depended on OS. Please find instruction pages [GRASS GIS Software Download](https://grass.osgeo.org/download/software/).


#### 2. Configuration and install python dependency
Find your GRASS GIS Application directory and confirm `GRASS7BIN_OS` is correct in catchment config.py.
``Note: If using GRASS docker image, do NOT need to change config.py``


If deploy on MAC OS, please initialize a GRASS project and set GRASSS Location(location name=h2Location) youself. Otherwise the application will not be runable. 

#### 3. Run script via python2
Install python dependency 
```
pip install -r requirements.txt
```
Then, the command is similar 
```
python App.py -d "97LanYang(5mDEM)(ArcGis)_(dig).asc" -c channel_lanyang.shp -p h2 -h 30 -w 200 -b "2711707;2704385;295588;285588"
```







## Efficiency Test
### Test case1:
- Story: compare running time for different parameters
- Device: MacPro 2017 late, I5, 16GB, SSD
- Input 97LanYang(5mDEM)
- Data import time: nearly 50 sec
- session1: 
    - CATCHMENT_THRESHOLD = 10000, CATCHMENT_MAX_SLOPE_LENGTH = 500000
    - Total Time: 4208.8 sec
    - Total Time for processing a 5km\*5km subset: 59.6 sec (still imported full converage 97LanYang DEM, nearly 50 sec cost)
- session2: 
    - CATCHMENT_THRESHOLD = 10000, CATCHMENT_MAX_SLOPE_LENGTH = 100000
    - Total Time: 2684.6 sec
    - Total Time for processing a 5km\*5km subset: 57.1 sec (same condition as Case1)

### Test case2
- Story: compare running time between docker and physical machine
- Device: MacPro 2017 late, I5, 16GB, SSD
- Input: 97LanYang(5mDEM)
- Region: subset a small region
- Parameter: CATCHMENT_THRESHOLD = 3000, CATCHMENT_MAX_SLOPE_LENGTH = 10000
- session1 physical machine: 
   - Total Time: 140.8 sec
- session2 docker: 
    - Total Time: 150.6 sec

## TODO
- [x] Automation to initialize a new GRASS project (#1)
- [x] Implement and test for Linux/Docker environement
- [ ] Log and Tests code

#1 In MAC OS, initial a GRASS project before execute is still needed 
