# -*- coding: utf-8 -*-
#!python2.7
import os, sys, subprocess, shutil, binascii, json, tempfile
import config


class GrassApiCaller(object):
    def __init__(self, work_dir='../storage'):
        print 'Initializing a computational session'
        # Define and manage layer name in GRASS session
        # When running gscript, avoids arbitrarily naming
        self.m_mymapset = 'PERMANENT'
        self.m_dem_layer = 'h2_dem'
        self.m_fill_layer = 'h2_fill'
        self.m_fill_dir_layer = 'h2_dir'
        self.m_preset_channel = 'h2_input_channel'
        self.m_preset_channel_buffer = 'h2_channel_buffer'
        self.m_preset_channel_buffer_r = 'h2_channel_buffer_r'
        self.m_watershed_dir = 'h2_water_dir'
        self.m_watershed_basin = 'h2_bas'
        self.m_watershed_basin_single = 'h2_bas_single'
        self.m_watershed_stream = 'h2_str'
        self.m_watershed_basin_v = 'h2_bas_v'
        self.m_watershed_stream_v = 'h2_str_v'
        self.m_channel_carve = 'h2_dem_carve'
        self.m_channel_gdf = None

        cwd = os.getcwd()
        storage_dir = work_dir
        self.inputloc = cwd + '/' + storage_dir + '/'

        if sys.platform.startswith('linux'):
            grass7bin = config.GRASS7BIN_LIN

            startcmd = [grass7bin, '--config', 'path']
            shell_enable = False
        elif sys.platform.startswith('win'):
            grass7bin = config.GRASS7BIN_WIN
            startcmd = [grass7bin, '--config', 'path']
            shell_enable = False
        elif sys.platform.startswith('darwin'):
            grass7bin = config.GRASS7BIN_MAC
            startcmd = grass7bin + ' --config path'
            shell_enable = True
        else:
            OSError('Platform not configured')

        p = subprocess.Popen(
            startcmd,
            shell=shell_enable,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        out, err = p.communicate()

        if p.returncode != 0:
            print >> sys.stderr, "ERROR: Cannot find GRASS GIS 7 start script (%s)" % startcmd
            sys.exit(-1)

        if sys.platform.startswith('win'):
            gisbase = out.strip('\n\r')
            os.environ['GRASS_SH'] = os.path.join(gisbase, 'mys', 'bin',
                                                  'sh.exe')
        else:
            gisbase = out.strip('\n')

        # Set GISBASE environment variable
        os.environ['GISBASE'] = gisbase
        # define GRASS-Python environment
        gpydir = os.path.join(gisbase, "etc", "python")
        sys.path.append(gpydir)

        # define GRASS DATABASE
        if sys.platform.startswith('win'):
            gisdb = os.path.join(os.getenv('APPDATA', 'grassdata'))
            string_length = 16
            location = binascii.hexlify(os.urandom(string_length))
        elif sys.platform.startswith('darwin'):
            gisdb = os.path.join(
                os.getenv('HOME', 'grassdata'), 'File', 'work', 'h2', 'grass')
            location = 'newLocation'
        elif sys.platform.startswith('linux'):
            #override for now with TEMP dir
            gisdb = os.path.join(tempfile.gettempdir(), 'grassdata')
            string_length = 16
            #Location/mapset:random names
            location = binascii.hexlify(os.urandom(string_length))

        # create location for docker
        if sys.platform.startswith('linux'):
            try:
                os.stat(gisdb)
            except:
                os.mkdir(gisdb)

            location_path = os.path.join(gisdb, location)

            #create new location
            startcmd = [grass7bin, '-c', 'epsg: 3826', '-e', location_path]

            print startcmd
            p = subprocess.Popen(
                startcmd,
                shell=shell_enable,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            out, err = p.communicate()
            if p.returncode != 0:
                print >> sys.stderr, 'ERROR: %s' % err
                print >> sys.stderr, 'ERROR: Cannot generate location (%s)' % startcmd
                sys.exit(-1)
            else:
                print 'Created location %s' % location_path

        # start grass session..
        import grass.script as gscript
        import grass.script.setup as gsetup
        self.gsetup = gsetup
        self.gscript = gscript
        mapset = self.m_mymapset
        gsetup.init(gisbase, gisdb, location, mapset)

    def import_dem(self, dem_file):
        print "=================="
        print 'Process: import DEM to GRASS'
        self.gscript.run_command(
            'r.in.gdal',
            input=self.inputloc + dem_file,
            output=self.m_dem_layer,
            flags='o',
            overwrite='True')
        self.gscript.run_command(
            'g.region',
            raster=self.m_dem_layer + '@' + self.m_mymapset,
            flags='p')

    def set_dem_boundary(self, dem_boundary_shp=None, dem_boundary=None):
        print "=================="

        if dem_boundary_shp != None:
            print 'Process: set computational boundary shp: '
            self.gscript.run_command(
                'v.in.ogr',
                input=self.inputloc + dem_boundary_shp,
                output='basin_boundary',
                overwrite=True)
            self.gscript.run_command(
                'g.region', vect='basin_boundary@' + self.m_mymapset)
        elif dem_boundary != None:
            print 'Process: set computational boundary: ' % dem_boundary
            self.gscript.run_command(
                'g.region',
                n=dem_boundary['n'],
                s=dem_boundary['s'],
                e=dem_boundary['e'],
                w=dem_boundary['w'])
        else:
            return

    def run_dig_preset_channel(self, shp_file, depth, width, layer='dem'):
        print "=================="
        print "Process: dig_preset_channel"

        if layer == 'dem':
            layer_name = self.m_dem_layer
        self.gscript.run_command(
            'v.in.ogr',
            input=self.inputloc + shp_file,
            output=self.m_preset_channel,
            overwrite=True)
        # r.carve
        self.gscript.run_command(
            'r.carve',
            vect=self.m_preset_channel + '@' + self.m_mymapset,
            rast=self.m_dem_layer + '@' + self.m_mymapset,
            out=self.m_channel_carve,
            depth=depth,
            width=width,
            overwrite='True')

    def run_fill_in(self):
        print "=================="
        print 'Process: DEM fill-dir algorithm'
        self.gscript.run_command(
            'r.fill.dir',
            input=self.m_channel_carve + '@' + self.m_mymapset,
            output=self.m_fill_layer,
            direction=self.m_fill_dir_layer,
            overwrite='True')

    def run_watershed_single(self, coordinates):
        # r.water.outlet - Creates watershed basins from a drainage direction map.
        print "=================="
        print 'Process: generate a Catchment according to a single point...'
        self.gscript.run_command(
            'r.water.outlet',
            input=self.m_watershed_dir + '@' + self.m_mymapset,
            coordinates=coordinates,
            output=self.m_watershed_basin_single,
            overwrite='True')
        print ''

    def run_watershed_global(self, threshold, max_slope_length):
        print "=================="
        print 'Process: generate Catchment (global mode)...'
        print 'Given Threshold: %f  ' % threshold
        print 'Given Max_Slope_length: %f  \n ' % max_slope_length

        # r.watershed --overwrite elevation=fill@pann depression=dir@pann threshold=1000 max_slope_length=1000 basin=bas stream=str
        self.gscript.run_command(
            'r.watershed',
            elevation=self.m_fill_layer + '@' + self.m_mymapset,
            #depression=self.m_fill_dir_layer + '@' + self.m_mymapset,
            threshold=threshold,
            max_slope_length=max_slope_length,
            basin=self.m_watershed_basin,
            drainage=self.m_watershed_dir,
            stream=self.m_watershed_stream,
            overwrite='True')

    def export_geojson(self, bas_out_file, str_out_file):
        print "=================="
        print 'Exporting Geojson'
        self.gscript.run_command(
            'r.to.vect',
            input=self.m_watershed_basin + '@' + self.m_mymapset,
            output=self.m_watershed_basin_v + '@' + self.m_mymapset,
            type='area',
            overwrite='True')

        self.gscript.run_command(
            'v.out.ogr',
            input=self.m_watershed_basin_v + '@' + self.m_mymapset,
            output=self.inputloc + bas_out_file,
            type='area',
            format='GeoJSON',
            overwrite='True')

        print 'Adding CRS tag and value (EPSG:3826) to basin geojson'
        with open(self.inputloc + bas_out_file) as f:
            data = json.load(f)
            data['crs'] = {"type": "name", "properties": {"name": "EPSG:3826"}}
        with open(self.inputloc + bas_out_file, 'w') as o:
            json.dump(data, o)

        print 'Exporting stream Geojson'
        self.gscript.run_command(
            'r.to.vect',
            input=self.m_watershed_stream + '@' + self.m_mymapset,
            output=self.m_watershed_stream_v + '@' + self.m_mymapset,
            type='area',
            overwrite='True')

        self.gscript.run_command(
            'v.out.ogr',
            input=self.m_watershed_stream_v + '@' + self.m_mymapset,
            output=self.inputloc + str_out_file,
            type='line',
            format='GeoJSON',
            overwrite='True')

        print 'Adding CRS tag and value (EPSG:3826) to stream geojson'
        with open(self.inputloc + str_out_file) as f:
            data = json.load(f)
            data['crs'] = {"type": "name", "properties": {"name": "EPSG:3826"}}
        with open(self.inputloc + str_out_file, 'w') as o:
            json.dump(data, o)
        print "Exporting GeoJSON process done!"
