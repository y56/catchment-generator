# -*- coding: utf-8 -*-
#!python2.7

# Environment
GRASS7BIN_WIN = ''
GRASS7BIN_LIN = 'grass'
GRASS7BIN_MAC = '~/../../Applications/GRASS-7.4.1.app/Contents/Resources/bin/grass74'
