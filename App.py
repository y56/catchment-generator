# -*- coding: utf-8 -*-
#!python2.7
import time, sys, getopt
from catchment import GrassApiCaller


def usage():
    print 'Usage:'
    print sys.argv[0] + '\n -d <dem_file_name>\n -c <channel_shapefile_name>\n \nOptional:\n -p <prefix name of ouput file, defaut is h2_>\n -t <threshold for catchment (default is 30000)>\n -m <max slope length of catchment (default is 1000)>\n -h <depth for channel (default is 20)>\n -w <width for channel in meter (default is 300)>\n -b <boundary coordinates for computation. Foramt should be "N;S;E;W">\n -s <boundary shp for computation>'


if __name__ == '__main__':

    # Input DEMO

    # look for arguments:
    if '-help' in sys.argv:
        usage()
        sys.exit(-1)

    if '-d' not in sys.argv and '--dem' not in sys.argv:
        usage()
        sys.exit(-1)
    if '-c' not in sys.argv and '--channel' not in sys.argv:
        usage()
        sys.exit(-1)

    try:
        argv = sys.argv[1:]
        print argv
        opts, args = getopt.getopt(argv, "d:c:p:h:w:b:s:", [
            "dem=", 'channel=', 'prefix=', 'height', 'width', 'boundary',
            'boundaryshp'
        ])
    except getopt.GetoptError as err:
        print '\nCommand line option error: ' + str(err)
        usage()
        sys.exit(-1)

    OUTPUT_PREFIX = "h2_"
    THRESHOLD = 30000
    MAX_SLOPE_LENGTH = 1000
    HEIGHT = 20
    WIDTH = 300
    BOUNDARY = [0, 0, 0, 0]
    BOUNDARY_SHP = None

    for opt, arg in opts:
        if opt in ("-d", "--dem"):
            DEM_INP_PATH = arg
            print arg
        elif opt in ("-c", "--channel"):
            CHANNEL_shp = arg
            print arg
        elif opt in ("-p", '--prefix'):
            OUTPUT_PREFIX = arg
            print arg
        elif opt in ("-t", '--threshold'):
            THRESHOLD = arg.split(';')
            print arg
        elif opt in ("-m", '--maxslope'):
            MAX_SLOPE_LENGTH = arg.split(';')
            print arg
        elif opt in ("-h", '--height'):
            HEIGHT = arg
            print arg
        elif opt in ("-w", '--width'):
            WIDTH = arg
            print arg
        elif opt in ("-s", '--boundaryshp'):
            BOUNDARY_SHP = arg
            print arg

    tStart = time.time()
    catchment_runner = GrassApiCaller(work_dir='../storage')

    catchment_runner.import_dem(DEM_INP_PATH)
    # 設定計算邊界

    if BOUNDARY[0] != 0:
        DEM_BOUNDARY = {
            "n": BOUNDARY[0],
            "s": BOUNDARY[1],
            "e": BOUNDARY[2],
            "w": BOUNDARY[3]
        }
        catchment_runner.set_dem_boundary(dem_boundary=DEM_BOUNDARY)
    elif BOUNDARY_SHP != None:

        catchment_runner.set_dem_boundary(dem_boundary_shp=BOUNDARY_SHP)
    # 下挖河道
    catchment_runner.run_dig_preset_channel(CHANNEL_shp, HEIGHT, WIDTH)
    # 挖地填平
    catchment_runner.run_fill_in()
    # 集水區

    catchment_runner.run_watershed_global(THRESHOLD, MAX_SLOPE_LENGTH)

    ##################### 測試中
    # catchment_runner.run_watershed_growing()
    #catchment_runner.run_watershed_single("291369.576617,2706673.48398")

    # Output DEMOs
    BASIN_OUT_PATH = OUTPUT_PREFIX + '_basin.geojson'
    STREAM_OUT_PATH = OUTPUT_PREFIX + '_stream.geojson'
    catchment_runner.export_geojson(BASIN_OUT_PATH, STREAM_OUT_PATH)

    print 'Processed Basin data: %s is writen' % BASIN_OUT_PATH
    print 'Done!'
    tEnd = time.time()
    print "This session costs %f sec" % (tEnd - tStart)
