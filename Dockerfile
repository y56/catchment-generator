# 
# FROM lisastillwell/py-grass
FROM mundialis/grass-gis-stable


# Container working directory /app
WORKDIR /app

# clone ./ into container /app
ADD ./ /app

# Python2.7 requirements.txt 
#RUN pip install -r requirements.txt

# Expose port
EXPOSE 80

# Environment variable
#ENV NAME World
ENTRYPOINT ["python"]

# Run CMD when container start app.py
CMD ["-V"]
#CMD ["python","example.py"]