# Work Note

In `~/h2beaver-public-y56/catchment-generator`,
I have
```=
-rw-rw-r-- 1 y56 y56 3482  五  25 02:56 App.py
drwxrwxr-x 2 y56 y56 4096  五  25 02:56 catchment/
drwxrwxr-x 5 y56 y56 4096  五  25 04:53 catchment-yilan-vector/
-rw-rw-r-- 1 y56 y56  393  五  25 06:36 Dockerfile
drwxrwxr-x 8 y56 y56 4096  五  25 02:56 .git/
-rw-rw-r-- 1 y56 y56   14  五  25 02:56 .gitignore
-rwxrwxrwx 1 y56 y56 1964  五  25 03:51 maven-release-poc.txt*
-rw-rw-r-- 1 y56 y56 8042  五  25 06:39 note.md
-rw-rw-r-- 1 y56 y56 4117  五  25 03:51 README.md
drwxrwxr-x 5 y56 y56 4096  五  25 04:49 river-yilan-vector/
drwxrwxr-x 5 y56 y56 4096  五  25 04:49 terrain-taiwan-raster_20m/
drwxrwxr-x 2 y56 y56 4096  五  25 02:56 tests/
```
And a `storage` dir, parallel to `catchment-generator` dir

## Docker --- DONE
We need `sudo`. 
If want to get rid of sudo, read https://docs.docker.com/install/linux/linux-postinstall/


Execute `sudo docker build -t catchment-generator .`
Get
```=
Sending build context to Docker daemon  498.7MB
Step 1/5 : FROM mundialis/grass-gis-stable
 ---> 8ef9afda02c9
Step 2/5 : WORKDIR /app
 ---> Using cache
 ---> 96b4a7d108b5
Step 3/5 : ADD ./ /app
 ---> 2fc5678859cb
Step 4/5 : ENTRYPOINT ["python"]
 ---> Running in 54b660a37a4a
Removing intermediate container 54b660a37a4a
 ---> b87e4d41365d
Step 5/5 : CMD ["-V"]
 ---> Running in 3b4346684337
Removing intermediate container 3b4346684337
 ---> 9fa2ac34936e
Successfully built 9fa2ac34936e
Successfully tagged catchment-generator:latest

```
The `Dockfile`  contains infomation needed for Docker to build.
I don't know how to write a dockfile

## Register --- DONE
Register new user (or login) in our h2beaver GitLab: https://140.124.61.32/ 
(may need to use Chrome or other brower allow in-secure https connection)
Done before.


## ssh key  --- DONE
prepare ssh-tunnel to be able to access 
Follow https://140.124.61.32/help/ssh/README#generating-a-new-ssh-key-pair
Command `ssh -T git@140.124.61.32` succeed.
```Welcome to GitLab, @y56!```

## Cloning data --- DONE
Clone the 3 repos into local
I put the three into `catchment-generator/`, by clone, with ssh

https://140.124.61.32/h2beaver/terrain-taiwan-raster_20m

https://140.124.61.32/h2beaver/river-yilan-vector

https://140.124.61.32/h2beaver/catchment-yilan-vector

## Linux command --- DONE
Try to do 
`ssh dev-user@140.124.61.32 -L8088:localhost:8088`

### ~~try_1, using 'git', fail~~

~~`ssh git@140.124.61.32 -L8088:localhost:8088`~~

> ~~PTY allocation request failed on channel 2~~
> ~~Welcome to GitLab, @y56!~~
> ~~Connection to 140.124.61.32 closed.~~

> ~~bind: Address already in use~~
> ~~channel_setup_fwd_listener_tcpip: cannot listen to port: 8088~~
> ~~Could not request local forwarding.~~
> ~~PTY allocation request failed on channel 0~~
> ~~Welcome to GitLab, @y56!~~
> ~~Connection to 140.124.61.32 closed.~~
### ~~try_2, using 'y56', fail~~
~~`ssh y56@140.124.61.32 -L8088:localhost:8088`~~
~~It asks me for password. I use the one by which I can log into `140.124.61.32` as `y56`. (Many checks. No typo.) But FAIL! QQ. I also tried blank, fail.~~
~~Server knows I am y56. So the ssh key does work. But no interactive shell for me.~~





### try_3, literally using 'dev-user', fail, QQ, DONE
`ssh dev-user@140.124.61.32 -L8088:localhost:8088`
It asks me for password. I tried blank, fail.
GOOD. I got the password.





### some other learning notes
#### ssh -L
```=
     -L [bind_address:]port:host:hostport
     -L [bind_address:]port:remote_socket
     -L local_socket:host:hostport
     -L local_socket:remote_socket
             Specifies that connections to the given TCP port or Unix socket on the local (client) host are to be forwarded to the given host
             and port, or Unix socket, on the remote side.  This works by allocating a socket to listen to either a TCP port on the local
             side, optionally bound to the specified bind_address, or to a Unix socket.  Whenever a connection is made to the local port or
             socket, the connection is forwarded over the secure channel, and a connection is made to either host port hostport, or the Unix
             socket remote_socket, from the remote machine.

             Port forwardings can also be specified in the configuration file.  Only the superuser can forward privileged ports.  IPv6
             addresses can be specified by enclosing the address in square brackets.

             By default, the local port is bound in accordance with the GatewayPorts setting.  However, an explicit bind_address may be used
             to bind the connection to a specific address.  The bind_address of “localhost” indicates that the listening port be bound for
             local use only, while an empty address or ‘*’ indicates that the port should be available from all interfaces.
```

#### 8088
port 8088 #Radan http

https://serverfault.com/questions/398462/what-is-radan-http

for use for proxies (along with 8000, 8080, 8888)

#### ssh -T
` -T      Disable pseudo-terminal allocation.`

https://stackoverflow.com/questions/17900760/what-is-pseudo-tty-allocation-ssh-and-github



## Step 5: try to access `http://localhost:8088` --- DONE
try access: http://localhost:8088, the 'Welcome to Apache Archiva' page

### ~~fail~~
~~I have to run a container first.~~
~~No, `http://localhost:8088` is not from docker, it is from ssh.~~

### interesting discovory
Since I used `nginx` previously to test whether my docker is workinhg well, the `http://localhost:8088` is somehow occupied by nginx's webpage.


This continues to happend on both Google Chrome and Firefox.
However, by using a incogniton/private page, I can reach the `Welcome to Apache Archiva` page.

And this still happens after I did `docker rmi` to remove the image of `nginx` (of courese no containers are running, checked by `docker container ls`) 

Before `ssh dev-user@140.124.61.32 -L8088:localhost:8088`, I have `This site can’t be reached` `localhost refused to connect`, but after I ssh, a normal page shows `ngnic` page and a incogniton page shows `Welcome to Apache Archiva` page.

![](https://i.imgur.com/du6S85A.png)







#### ~~fail 1~~
~~`sudo docker run -d -p 80:80 --name webserver002 catchment-generator`~~
~~`569f8f944ea7d7773aeb2efb8f4bd09aa45ea315ab4b3be91c3da51dd759523c`~~
#### ~~fail 2~~
~~`sudo docker run catchment-generator`~~

~~`Python 2.7.15rc1`~~

### useful commands
#### `sudo docker container list`
```=
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES

```
So... I am not running any containers.


#### `sudo docker images`
```=
REPOSITORY                   TAG                 IMAGE ID            CREATED             SIZE
catchment-generator          latest              8f4ee3d4231a        2 minutes ago       1.94GB
<none>                       <none>              26e40f96fd8f        43 minutes ago      1.94GB
<none>                       <none>              889561880be9        3 hours ago         1.45GB
mundialis/grass-gis-stable   latest              8ef9afda02c9        5 days ago          1.45GB
nginx                        latest              53f3fd8007f7        2 weeks ago         109MB
hello-world                  latest              fce289e99eb9        4 months ago        1.84kB

```
#### try editing Dockfile
`#` is for command in dockfiles.
I try to enable `EXPOSE 80`.
So I modify the dockfile into
```dockerfile=
# 
# FROM lisastillwell/py-grass
FROM mundialis/grass-gis-stable


# Container working directory /app
WORKDIR /app

# clone ./ into container /app
ADD ./ /app

# Python2.7 requirements.txt 
#RUN pip install -r requirements.txt

# Expose port
EXPOSE 80

# Environment variable
#ENV NAME World
ENTRYPOINT ["python"]

# Run CMD when container start app.py
CMD ["-V"]
#CMD ["python","example.py"]
```
build again and try the belows
```=
Sending build context to Docker daemon  498.7MB
Step 1/6 : FROM mundialis/grass-gis-stable
 ---> 8ef9afda02c9
Step 2/6 : WORKDIR /app
 ---> Using cache
 ---> 96b4a7d108b5
Step 3/6 : ADD ./ /app
 ---> Using cache
 ---> edd547f85003
Step 4/6 : EXPOSE 80
 ---> Using cache
 ---> 4e318a451fd5
Step 5/6 : ENTRYPOINT ["python"]
 ---> Using cache
 ---> 898c76dd075c
Step 6/6 : CMD ["-V"]
 ---> Using cache
 ---> 8f4ee3d4231a
Successfully built 8f4ee3d4231a
Successfully tagged catchment-generator:latest
```

##### fail 1
`sudo docker run -d -p 80:80 --name webserver001 catchment-generator`
`70e3b34cd9e6f02418264cdf735c33acc0c1417053517dff428ae30d3d743820`
##### fail 2
`sudo docker run catchment-generator`

`Python 2.7.15rc1`


#### BTW
I once used 
`docker run -d -p 80:80 --name webserver nginx`
to test.
With 
```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
1089912c7ae8        nginx               "nginx -g 'daemon of…"   23 minutes ago      Up 23 minutes       0.0.0.0:8088->80/tcp   webserver

```
And `http://localhost:8088` works.


## Step 6: DONE
create a settings.xml file in ~/.m2, to store archiva username and password
They are **literally** `h2` and `h2password`. 
```xml=
<settings>
  <servers>
    <server>
      <id>archiva.internal</id>
      <username>h2</username>
      <password>h2password</password>
    </server>
    <server>
      <id>archiva.snapshots</id>
      <username>h2</username>
      <password>h2password</password>
    </server>
  </servers>
</settings>
```
 
## Step 7: DONE
> goto terrain-taiwan-raster_20m folder
> update version defined in pom.xml to 2016-1-{some number not use yet}
> run the command:
> mvn deploy
> 
> after deploy, the new version should be added into archiva, can be checked in this url:
> http://localhost:8088/#artifact/h2beaver/terrain-taiwan-raster_20m
### 1
`mvn install` in `catchment-generator/river-yilan-vector`
OK
### 2
- edit `catchment-generator/catchment-yilan-vectorassembly/h2.xml` to uncomment the section 
- `mvn install` in `catchment-generator/catchment-yilan-vector`
- 
OK
### 3
- edit `catchment-generator/terrain-taiwan-raster_20m/pom.xml` into
```xml=
<groupId>h2beaver</groupId>
<artifactId>terrain-taiwan-raster_20m</artifactId>
<version>2016-1-1</version>
```
- Run `mvn deploy` in `catchment-generator/terrain-taiwan-raster_20m`
- I create `2016-1-1-TEST` and `2016-1-1` as tests.

OK

## Step 8: DONE
> goto catchment-yilan-vector folder
> update dependency version of terrain-taiwan-raster_20m to the new version of Step 7
> run the command:
> mvn clean compile
> 
### 
- change `catchment-generator/catchment-yilan-vector/pom.xml` into
```=
<groupId>h2beaver</groupId>
<artifactId>terrain-taiwan-raster_20m</artifactId>
<version>2016-1-1-TEST</version>
```
- `mvn clean compile`

OK

## Step 9: YET
> try to use QGIS or other GIS to show the two layers:
> catchment-yilan-vector/workspace/river-yilan-vector-resources-zip/river.geojson
> catchment-yilan-vector/workspace/h2_basin.geojson

Can't find `catchment-yilan-vector/workspace/h2_basin.geojson`

